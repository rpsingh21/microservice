#!/bin/bash

export GO_PATH=~/go
export PATH=$PATH:/$GO_PATH/bin
protoc greet.proto --go_out=plugins=grpc:.