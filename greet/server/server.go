package main

import (
	"context"
	"flag"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"time"

	"gitlab.com/rpsingh21/microservice/greet/greetProto"
	"google.golang.org/grpc"
)

type gserver struct{}

var logger = log.New(os.Stdout, "[gRPC] ", log.LstdFlags|log.Lshortfile)

func (*gserver) Greet(ctx context.Context, req *greetProto.GreetRequest) (*greetProto.GreetResponse, error) {
	logger.Printf("Greet function invoked with %v\n", req)
	firstName := req.Greeting.GetFirstName()
	lastName := req.GetGreeting().GetLastName()

	result := "Hello! " + firstName + " " + lastName

	res := &greetProto.GreetResponse{Result: result}
	return res, nil
}

func (*gserver) GreetManyTimes(req *greetProto.GreetRequest, stream greetProto.GreetService_GreetManyTimesServer) error {
	logger.Printf("GreetManyTimes function invoked with %v\n", req)
	firstName := req.Greeting.GetFirstName()
	lastName := req.GetGreeting().GetLastName()

	result := "Hello! " + firstName + " " + lastName

	for i := 0; i < 10; i++ {
		temp := result + " " + strconv.Itoa(i+1)
		res := &greetProto.GreetResponse{Result: temp}
		stream.Send(res)
		time.Sleep(200 * time.Millisecond)
	}
	return nil
}

func (*gserver) BiGreet(stream greetProto.GreetService_BiGreetServer) error {
	logger.Printf("Bidireaction greet function invoked with \n")
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			logger.Fatalf("Falst dutiong reading stram : %v", err)
			return err
		}

		result := "Hello! " + req.Greeting.GetFirstName() + " " + req.Greeting.GetLastName()
		senderr := stream.Send(&greetProto.GreetResponse{
			Result: result,
		})
		if senderr != nil {
			logger.Fatalf("Falt duction sending bi resqponse : %v", err)
			return senderr
		}
	}
}

func main() {
	bindAdds := flag.String("ba", "0.0.0.0:5000", "gRPC server address")
	flag.Parse()
	logger.Printf("Server starting on %s", *bindAdds)

	lis, err := net.Listen("tcp", *bindAdds)
	if err != nil {
		logger.Fatalf("Faild to start server : %v", err)
	}

	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)

	greetProto.RegisterGreetServiceServer(s, &gserver{})

	if err := s.Serve(lis); err != nil {
		logger.Fatalf("Faild to start gRPC service : %v", err)
	}
}
