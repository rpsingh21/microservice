package main

import (
	"context"
	"flag"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/rpsingh21/microservice/greet/greetProto"
	"google.golang.org/grpc"
)

var logger = log.New(os.Stdout, "[gRPC] ", log.LstdFlags|log.Lshortfile)

func main() {

	bindAdds := flag.String("ba", "0.0.0.0:5000", "gRPC server address")
	flag.Parse()
	logger.Printf("Server starting on %s", *bindAdds)

	opts := grpc.WithInsecure()
	cc, err := grpc.Dial(*bindAdds, opts)
	if err != nil {
		logger.Fatalf("Faild to dial gRPC server : %v", err)
	}
	defer cc.Close()

	c := greetProto.NewGreetServiceClient(cc)

	// doUnary(c)
	// doServerStreaming(c)
	doBiDirStaream(c)
}

func doUnary(c greetProto.GreetServiceClient) {
	logger.Printf("stating to sernd unary Grpc")
	req := &greetProto.GreetRequest{
		Greeting: &greetProto.Greeting{
			FirstName: "Testing",
			LastName:  "gRPC",
		},
	}

	res, err := c.Greet(context.Background(), req)
	if err != nil {
		logger.Fatalf("Error while calling Greet RPC : %v", err)
	}
	logger.Printf("gRPC responsce : %s", res.Result)
}

func doServerStreaming(c greetProto.GreetServiceClient) {
	logger.Printf("statring streaming server")
	req := &greetProto.GreetRequest{
		Greeting: &greetProto.Greeting{
			FirstName: "Testing",
			LastName:  "gRPC",
		},
	}
	resStream, err := c.GreetManyTimes(context.Background(), req)
	if err != nil {
		logger.Fatalf("Error while calling server stream gRPC : %v", err)
	}
	for {
		mgs, err := resStream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			logger.Fatalf("Error while calling server stream gRPC : %v", err)
		}
		log.Printf("Response from GreetManyTimes: %v", mgs.GetResult())
	}
}

func doBiDirStaream(c greetProto.GreetServiceClient) {
	logger.Printf("Starting to do a BiDi Streaming RPC...")
	stream, err := c.BiGreet(context.Background())
	if err != nil {
		log.Fatalf("Error while create stream : %v", err)
		return
	}

	waitc := make(chan struct{})

	// we send a bunch of messages to the client (go routine)
	go func() {
		// function to send a bunch of messages
		for i := 0; i < 5; i++ {
			req := &greetProto.GreetRequest{
				Greeting: &greetProto.Greeting{
					FirstName: "Testing",
					LastName:  strconv.Itoa(i),
				},
			}
			stream.Send(req)
			time.Sleep(1000 * time.Millisecond)
		}
		stream.CloseSend()
	}()

	// we receive a bunch of messages from the client (go routine)
	go func() {
		// function to receive a bunch of messages
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("Error while receiving: %v", err)
				break
			}
			logger.Printf("Received: %v\n", res.GetResult())
		}
		close(waitc)
	}()

	// block until everything is done
	<-waitc
}
