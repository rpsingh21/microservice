package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/rpsingh21/microservice/product-api/handlers"
)

func main() {
	apiLogger := log.New(os.Stdout, "[ProductAPI] ", log.LstdFlags|log.Llongfile)
	bindAddr := "0.0.0.0:8000"
	args := os.Args
	if len(args) > 1 {
		bindAddr = args[1]
	}

	ph := handlers.NewProduct(apiLogger)

	router := mux.NewRouter()
	router.Handle("/", ph).Methods(http.MethodGet, http.MethodPost)
	router.Handle("/{id:[0-9]+}", ph).Methods(http.MethodPut, http.MethodDelete)
	router.Use(mux.CORSMethodMiddleware(router))

	server := http.Server{
		Addr:         bindAddr,          // configure the bind address
		Handler:      router,            // set the default handler
		ErrorLog:     apiLogger,         // set the logger for the server
		ReadTimeout:  5 * time.Second,   // max time to read request from the client
		WriteTimeout: 10 * time.Second,  // max time to write response to the client
		IdleTimeout:  120 * time.Second, // max time for connections using TCP Keep-Alive
	}

	go func() {
		apiLogger.Println("Staring server on", bindAddr)
		if err := server.ListenAndServe(); err != nil {
			apiLogger.Printf("Error starting server: %s\n", err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	sig := <-c
	apiLogger.Println("Got Signal", sig)

	// gracefully shutdown the server, waiting max 30 seconds for current operations to complete
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	server.Shutdown(ctx)
}
