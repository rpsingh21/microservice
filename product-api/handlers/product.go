package handlers

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/rpsingh21/microservice/product-api/models"
	"gitlab.com/rpsingh21/microservice/product-api/repositories"
)

// ProductHandler handel product repalted query
type ProductHandler struct {
	logger *log.Logger
}

// NewProduct return new product hander object
func NewProduct(l *log.Logger) *ProductHandler {
	return &ProductHandler{l}
}

func (ph *ProductHandler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	ph.logger.Printf("Handel [%s] Request", r.Method)
	switch r.Method {
	case http.MethodGet:
		ph.getProducts(rw, r)
	case http.MethodPost:
		ph.createProduct(rw, r)
	case http.MethodPut:
		ph.updateProduct(rw, r)
	case http.MethodDelete:
		ph.deleteProduct(rw, r)
	default:
		rw.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (ph *ProductHandler) getProducts(rw http.ResponseWriter, r *http.Request) {
	ls := repositories.GetProducts()
	err := ls.ToJSON(rw)
	if err != nil {
		ph.logger.Fatal("Unable to marshal json")
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
		return
	}
}

func (ph *ProductHandler) createProduct(rw http.ResponseWriter, r *http.Request) {
	product := &models.Product{}
	if err := product.FromJSON(r.Body); err != nil {
		ph.logger.Fatal("Unable to marshal json")
		http.Error(rw, "Unable to marshal json", http.StatusBadRequest)
		return
	}
	if err := product.Validate(); err != nil {
		ph.logger.Println("[ERROR] validating product", err)
		http.Error(rw,
			fmt.Sprintf("Error validating product: %s", err),
			http.StatusBadRequest,
		)
		return
	}
	repositories.AddProduct(product)
}

func (ph *ProductHandler) updateProduct(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])

	product := &models.Product{}
	if err := product.FromJSON(r.Body); err != nil {
		http.Error(rw, "Unable to unmarshal json", http.StatusBadRequest)
		return
	}

	if err := product.Validate(); err != nil {
		ph.logger.Println("[ERROR] validating product", err)
		http.Error(rw,
			fmt.Sprintf("Error validating product: %s", err),
			http.StatusBadRequest,
		)
		return
	}

	err := repositories.UpdateProduct(id, product)
	if err == repositories.ErrProductNotFound {
		http.Error(rw, "Product not found", http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, "Product not found", http.StatusInternalServerError)
		return
	}
}

func (ph *ProductHandler) deleteProduct(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])

	if err := repositories.Delete(id); err != nil {
		http.Error(rw, "Not Found", http.StatusNotFound)
		return
	}
}

// MiddlewareValidateProduct middleware for all product handeler request
func (ph ProductHandler) MiddlewareValidateProduct(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		product := models.Product{}

		if err := product.FromJSON(r.Body); err != nil {
			ph.logger.Println("[ERROR] deserializing product", err)
			http.Error(rw, "Error reading product", http.StatusBadRequest)
		}

		// add the product to the context
		ctx := context.WithValue(r.Context(), HTTPBody{}, product)
		r = r.WithContext(ctx)

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(rw, r)
	})
}
