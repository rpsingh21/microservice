package models_test

import (
	"testing"

	"gitlab.com/rpsingh21/microservice/product-api/models"
)

func TestCheckValidation(t *testing.T) {
	p := &models.Product{
		Name:        "nics",
		Price:       1.00,
		Description: "hello",
		SKU:         "abs",
	}

	err := p.Validate()

	if err != nil {
		t.Fatal(err)
	}
}
