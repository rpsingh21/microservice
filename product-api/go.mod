module gitlab.com/rpsingh21/microservice/product-api

go 1.16

require (
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/leodido/go-urn v1.2.1
)
