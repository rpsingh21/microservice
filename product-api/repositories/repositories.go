package repositories

// Repositories interface
type Repositories interface {
	ToJSON() error
	Get() Repositories
	Update() error
	Delete() error
}
