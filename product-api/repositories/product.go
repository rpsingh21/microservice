package repositories

import (
	"encoding/json"
	"fmt"
	"io"
	"time"

	"gitlab.com/rpsingh21/microservice/product-api/models"
)

// Products types contain list of product
type Products []*models.Product

// ToJSON from json containt
func (ps *Products) ToJSON(w io.Writer) error {
	encoder := json.NewEncoder(w)
	return encoder.Encode(ps)
}

// GetProducts return all list of prodcut
func GetProducts() Products {
	return productList
}

// AddProduct adding new prodcut
func AddProduct(p *models.Product) {
	p.ID = getNextProductID()
	productList = append(productList, p)
}

// UpdateProduct for updating existing product
func UpdateProduct(id int, p *models.Product) error {
	_, pos, err := findProduct(id)
	if err != nil {
		return err
	}
	p.ID = id
	productList[pos] = p
	return nil
}

// Delete : delete prodcut by id
func Delete(id int) error {
	_, idx, err := findProduct(id)
	if err != nil {
		return err
	}
	productList = append(productList[:idx], productList[idx+1])
	return nil
}

// ErrProductNotFound panic ducring product not found
var ErrProductNotFound = fmt.Errorf("Product not found")

func findProduct(id int) (*models.Product, int, error) {
	for i, p := range productList {
		if p.ID == id {
			return p, i, nil
		}
	}
	return nil, 1, ErrProductNotFound
}

func getNextProductID() int {
	p := productList[len(productList)-1]
	return p.ID + 1
}

// productList is a hard coded list of products for this
// example data source
var productList = []*models.Product{
	{
		ID:          1,
		Name:        "Latte",
		Description: "Frothy milky coffee",
		Price:       2.45,
		SKU:         "abc323",
		CreatedOn:   time.Now().UTC().String(),
		UpdatedOn:   time.Now().UTC().String(),
	},
	{
		ID:          2,
		Name:        "Espresso",
		Description: "Short and strong coffee without milk",
		Price:       1.99,
		SKU:         "fjd34",
		CreatedOn:   time.Now().UTC().String(),
		UpdatedOn:   time.Now().UTC().String(),
	},
}
